# Response of Pacific halibut (*Hippoglossus stenolepis*) to future climate scenarios in the Northeast Pacific Ocean

__Main authors:__  Patrick Thompson, Chris Rooper  
__Contributors:__ Jessica Nephin, Amber Holdsworth, Sarah Davies, Ashley Park, Beatrice Proudfoot, Devin Lyons, , Karen Hunter, Emily Rubidge, Angelica Pena, Jim Christian  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: patrick.thompson@dfo-mpo.gc.ca

This repo contains the code to produce the analysis presented in the manuscript.

## Abstract  
Pacific halibut (*Hippoglossus stenolepis*) are a large-bodied species of flatfish that are important culturally, economically, and as a key predator in marine systems in the USA and Canada. The species has a wide distribution, and complex life history including large-scale migrations to spawn and feed, making it potentially susceptible to climate change impacts. We examined the potential changes in halibut distribution and relative abundance that may arise from changing temperature and dissolved oxygen concentrations using species distribution models (SDMs) and future climate scenarios downscaled by two regional ocean models. SDMs were fit with both environmental variables (depth, near-bottom temperature and dissolved oxygen concentration) and spatial random field components (representing unknown habitat-related variables). The best-fitting models, trained on data from 2009 to 2013, were able to account for 33% and 53% of the variation in small and large halibut catch-per-unit-effort in the annual set-line survey data from 2014-2020. The results suggest that the response of Pacific halibut to climate change in British Columbia and Washington waters is likely to depend on changes in dissolved oxygen concentration. Pacific halibut appear sensitive to changes in dissolved oxygen, yet relatively tolerant to increases in temperature. Projections for 2046-2065 period suggest that future decreases in near-bottom dissolved oxygen in shallow waters that small halibut inhabit are likely to result in moderate decreases in relative abundance. Projected changes in relative abundance are less certain for large Pacific halibut, due to disagreement in the regional ocean models near-bottom dissolved oxygen projections at mid depths (300-600 m) where large Pacific halibut are more common. Overall, the relative abundance of Pacific halibut is expected to decrease in most areas of British Columbia. Future management strategies will need to account for the projected changes in distribution and abundance and their uncertainty. 

